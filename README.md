# README #

A simple server/client program written in Java. Based on Chapter 15 from the book* Head First Into Java*.

To test, start one instance of the server, and two instances of the client.