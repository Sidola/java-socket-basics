package workingExample;

import java.io.Serializable;

public class Message implements Serializable {
	
	private String message;
	
	/**
	 * Sets a new message
	 * @param message
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	
	/**
	 * Returns the message
	 * @return
	 */
	public String getMessage() {
		return message;
	}
}
