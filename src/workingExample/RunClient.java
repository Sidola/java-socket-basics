package workingExample;

import java.util.Scanner;

import workingExample.client.Client;

public class RunClient {

	public static void main(String[] args) {
		
		Client client = new Client();
		Scanner scanner = new Scanner(System.in);
	
		// Connect to the server
		client.connect();
		
		// Loop and get input
		while(true) {
			Message message = new Message(); 
			
			System.out.print("Enter a message: ");
			String input = scanner.nextLine();
			message.setMessage(input);
			
			System.out.println("Sending message...");
			client.sendMessage(message);
			
			System.out.println("Waiting for message from the server...");
			Message retMessage = new Message();
			retMessage = (Message) client.getMessage();
			
			System.out.println("Message received: " + retMessage.getMessage());
		}
		
		
	}
	
}
