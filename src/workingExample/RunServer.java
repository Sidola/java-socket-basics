package workingExample;

import workingExample.server.Server;

public class RunServer {

	public static void main(String[] args) {
		
		Server server = new Server();
		server.start();
		
	}
	
}
