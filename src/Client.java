import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;


public class Client {
	
	/**
	 * Inner class to handle all incoming messages
	 */
	private class IncomingReader implements Runnable {
		
		/**
		 * This method will keep running until we receive null
		 * It's responsible for printing all incoming messages
		 */
		public void run() {			
			String msg;
			try {
				// Keep reading until we receive null
				while ((msg = reader.readLine()) != null) {
					System.out.println(">> " + msg);
				}
			} catch (Exception e) { e.printStackTrace();}
		}
	}
	
	private Socket socket;
	private String userName;
	private PrintWriter writer;
	private BufferedReader reader;
	private Scanner scanner = new Scanner(System.in);
	
	/**
	 * Connects to the server and instantiates the reader and writer
	 * 
	 * @param ip - IP of the server
	 * @param port - The port which the server is running on
	 */
	private void connectToServer(String ip, int port) {
		try {
			// Open a new socket
			socket = new Socket(ip, port);
			
			// Create a reader
			InputStreamReader streamReader = new InputStreamReader(socket.getInputStream());
			reader = new BufferedReader(streamReader);
			
			// Create a writer
			writer = new PrintWriter(socket.getOutputStream(), true);
			
			// Confirm connection
			System.out.println("Established connection with the server.");
		} catch (Exception e) { e.printStackTrace(); }
	}
	
	/**
	 * Asks the user for a username
	 * @return A string with the username
	 */
	private String getUserName() {
		System.out.print("Enter your username: ");
		String name = scanner.nextLine();
		return name;
	}
	
	/**
	 * Main client logic
	 * 
	 * This method starts by getting a username from the user.
	 * Then it connects to the server.
	 * 
	 * After that it creates a new thread to handle all incoming messages.
	 * Once that is done it will go into an infinite loop, accepting input from the user.
	 */
	public void go() {
		userName = getUserName();
		connectToServer("127.0.0.1", 4242);
		
		// Start a new thread to handle incoming messages
		Thread t = new Thread( new IncomingReader() );
		t.start();

		String msg;
		while (true) {
			msg = userName + ": " + scanner.nextLine();
			writer.println(msg);
		}
	}
	
	/**
	 * Main method, responsible for starting the client
	 * @param args
	 */
	public static void main(String[] args) {
		Client client = new Client();
		client.go();
	}
	
}
