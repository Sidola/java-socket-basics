import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;


public class Server {

	// Arraylist with writers to all connected clients
	ArrayList<PrintWriter> clientOutputStreams;
	
	/**
	 * Inner class to handle all connected clients
	 */
	private class ClientHandler implements Runnable {
		
		private BufferedReader reader;
		private InputStreamReader isReader;
		private PrintWriter writer;

		/**
		 * Constructor
		 * @param clientSocket
		 * @param clientWriter
		 */
		ClientHandler(Socket clientSocket, PrintWriter clientWriter) {
			try {
				// Get the inputstream for this client
				isReader = new InputStreamReader(clientSocket.getInputStream());
				reader = new BufferedReader(isReader);
				
				// Save the writer for this client
				this.writer = clientWriter;				
			} catch (Exception e) { e.printStackTrace(); }
		}
		
		/**
		 * This method will take all the input the client sends and relay it to
		 * all other connected clients
		 */
		public void run() {
			String msg;
			try {
				// Stay in this loop until we receive a null message
				while ((msg = reader.readLine()) != null) {
					// Relay the message to all connected clients
					pushToClients(msg, writer);
				}
			} catch (Exception e) { e.printStackTrace(); }
		}
		
	}

	/**
	 * Relays the given message to all clients on the network, except itself
	 * @param msg
	 * @param sendingClient
	 */
	public void pushToClients(String msg, PrintWriter sendingClient) {
		for (PrintWriter printWriter : clientOutputStreams) {
			// Don't send the message to the client who sent it
			if (!printWriter.equals(sendingClient)) {
				printWriter.println(msg);				
			}
		}
		
	}
	
	/**
	 * Main server logic
	 * 
	 * This method is responsible for connecting with new clients and giving them
	 * their own thread
	 */
	public void go() {
		clientOutputStreams = new ArrayList<PrintWriter>();
		
		try {
			ServerSocket serverSocket = new ServerSocket(4242);
			
			while(true) {
				// Wait for a client to connect
				Socket clientSocket = serverSocket.accept();
				
				// Create a writer for that client
				PrintWriter writer = new PrintWriter(clientSocket.getOutputStream(), true);
				// Store the client in our array
				clientOutputStreams.add(writer);
				
				// Create a new thread for the client
				Thread t = new Thread( new ClientHandler(clientSocket, writer) );
				t.start();
				
				// Confirm a new connection in the console
				System.out.println("A new client connected.");
			}			
		} catch (Exception e) { e.printStackTrace(); }
	}

	/**
	 * Main method, responsible for starting the server
	 * @param args
	 */
	public static void main(String[] args) {
		Server server = new Server();
		server.go();
	}
	
}











